---
description: >-
  This page is a comprehensive guide to the technical terminology used in the
  field of data backup and recovery.
---

# Glossary of Terms

Here, you will find an extensive list of terms related to backup and recovery, along with their definitions and explanations.&#x20;

Our glossary will help you navigate the technical jargon associated with backup and recovery.

Our list includes terms related to backup solutions, storage devices, data recovery, disaster recovery, and more. We have compiled this glossary to help you better understand the concepts and technologies involved in protecting and recovering your data.

Browse through our list of terms and definitions, and feel free to share your feedback or suggestions with us. We are committed to helping you understand the terminology associated with backup and recovery, so be sure to check back often for new terms and definitions.

<details>

<summary>A</summary>

[Archive](a/archive.md)

[Active backup for Office 365](a/active-backup-for-office-365.md)

[AWS Backup](a/aws-backup.md)

[Active Directory](a/active-directory.md)

[Agent](a/active-directory.md)

</details>

<details>

<summary>B</summary>

[Backup](b/backup.md)

[Backup and Recovery](b/backup-and-recovery.md)

[Backup as a service](b/backup-as-a-service.md)

[Bare-metal backup](b/bare-metal-backup.md)

[Backup repository](b/backup-repository.md)

[Backup schedule](b/backup-schedule.md)



</details>

<details>

<summary>C</summary>

[Cloud Backup](c/cloud-backup.md)

[Continuous Data Protection (CDP)](c/continuous-data-protection-cdp.md)

[Compression](c/compression.md)

[Consistency check](c/consistency-check.md)

[Cold Backup](c/cold-backup.md)

</details>

<details>

<summary>D</summary>

[Data Deduplication](d/data-deduplication.md)

[Disaster Recovery (DR)](d/disaster-recovery-dr.md)

[Differential Backup](d/differential-backup.md)

[Disk-to-Disk (D2D) Backup](d/disk-to-disk-d2d-backup.md)



</details>

<details>

<summary>E</summary>

[Encryption](e/encryption.md)

[Endpoint Backup](e/endpoint-backup.md)

[Erasure Coding](e/erasure-coding.md)

[Export/Import](e/export-import.md)

[Enterprise Backup Software](e/enterprise-backup-software.md)

</details>

<details>

<summary>F</summary>

[Full Backup](f/full-backup.md)

[Failover](f/failover.md)

[File-Level Backup](f/file-level-backup.md)

[File Sync and Share](f/file-sync-and-share.md)

[Fireproof and Waterproof Storage](f/fireproof-and-waterproof-storage.md)

</details>

<details>

<summary>G</summary>

[Grandfather-Father-Son (GFS)](g/grandfather-father-son-gfs.md)

[Granular Recovery](g/granular-recovery.md)

[Geographically Dispersed Backup](g/geographically-dispersed-backup.md)

[Ghost Imaging](g/ghost-imaging.md)

[Global Deduplication](g/global-deduplication.md)

</details>

<details>

<summary>H</summary>

[Hybrid Backup](h/hybrid-backup.md)

[Hot Backup](h/hot-backup.md)

[High Availability (HA)](h/high-availability-ha.md)

[Hard Disk Drive (HDD)](h/hard-disk-drive-hdd.md)

[Hybrid Cloud Backup](h/hard-disk-drive-hdd.md)

</details>

<details>

<summary>I</summary>

[Incremental Backup](i/incremental-backup.md)

[Image-based Backup](i/image-based-backup.md)

[Instant Recovery](i/instant-recovery.md)

[Integrity Check](i/instant-recovery.md)

[Infrastructure as a Service (IaaS)](i/instant-recovery.md)

</details>
